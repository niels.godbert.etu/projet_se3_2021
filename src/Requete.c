#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../includes/open.h"


struct cell
{
  char * val;
  struct cell * suiv;
};

void ajout_tete(struct cell **p, char *al) {

  struct cell *temp = NULL;
  temp = malloc(sizeof(struct cell));
  temp->val = al; // Ici 2 ptr donc pas d'utilisation de strcpy
  temp->suiv = *p;
  *p = temp;
}

// Requetes non fonctionnelles

/*
void most_delayed_flights(Hashtable Vols,struct cellule_airline* air)
{

  int max1=0,max2=0,max3=0,max4=0,max5=0,val=0,cpt=0;
  struct flight* ref1,*ref2,*ref3,*ref4,*ref5;

  struct cellule_airline* cell= air;

  while(cpt!=NBCOMPAGNIE){
    int index=hash_airline(cell-> info-> iATA_CODE_AIRLINE);
    struct cellule_vol* Newcell=Vols[index];

    while (Newcell!=NULL) {
      if (Newcell-> info->DEP_DELAY==0 || Newcell-> info->ARR_DELAY==0) val=0;
      else  val=Newcell-> info->DEP_DELAY + Newcell-> info->ARR_DELAY;
      if (val>=max1)
      {
        max1=val;
        ref1=Newcell-> info;
      }
      else
      {
        if (val>=max2) {
          max2=val;
          ref2=Newcell-> info;
        }
        else{);
          if (val>=max3)
          {
            max3=val;
            ref3=Newcell-> info;
          }
          else
          {
            if (val>=max4)
            {
              max4=val;
              ref4=Newcell-> info;
            }
            else
            {
              if (val>=max5)
              {
                max5=val;
                ref5=Newcell-> info;
              }
            }
          }
        }
      }
      Newcell=Newcell->suiv;
    }
    cell=cell->suiv;
    cpt++;
  }
  printf("1er  : %s \n",&(ref1) );
  printf("2eme : %s \n",&(ref2) );
  printf("3eme : %s \n",&(ref3) );
  printf("4eme : %s \n",&(ref4) );
  printf("5eme : %s \n",&(ref5) );
}

void most_delayed_airline(Hashtable Vols,struct cellule_airline* air)
{
  int max1=0,max2=0,max3=0,max4=0,max5=0,val=0,compte=0;
  struct airline *ref1,*ref2,*ref3,*ref4,*ref5;
  struct cellule_airline* cell= air;

  while(compte!=NBCOMPAGNIE)
  {
    int val=0,tot=0,cpt=0,moy=0;
    int index=hash_airline(cell-> info-> iATA_CODE_AIRLINE);
    struct cellule_vol* Newcell=Vols[index];

    while (Newcell!=NULL)
    {
      if (Newcell-> info->DEP_DELAY==0 || Newcell-> info->ARR_DELAY==0)  val=0;
      else
      {
        val=Newcell-> info->DEP_DELAY + Newcell-> info->ARR_DELAY;
        tot+=val;
        cpt+=1;
      }
      Newcell=Newcell->suiv;
    }
    moy=tot/cpt;
    if (moy>=max1) {
      max1=moy;
      ref1=cell-> info;
    }
    else{
      if (moy>=max2)
      {
        max2=moy;
        ref2=cell-> info;
      }
      else{
        if (moy>=max3)
        {
          max3=moy;
          ref3=cell-> info;
        }
        else{
          if (moy>=max4)
          {
            max4=moy;
            ref4=cell-> info;
          }
          else{
            if (moy>=max5) {
              max5=moy;
              ref5=cell-> info;
            }
          }
        }
      }
    }

    cell=cell->suiv;
    compte++;
  }

}

void delayed_airline(char* IATACODE,Hashtable Vols)
{
  int val=0,tot=0,cpt=0,moy=0;
  int index=hash_airport(IATACODE);
  struct cellule_vol* Newcell = Vols[index];

  while (Newcell!=NULL)
  {
    if (Newcell-> info->DEP_DELAY==0 || Newcell-> info->ARR_DELAY==0) val=0;
    else
    {
      val=Newcell-> info->DEP_DELAY + Newcell-> info->ARR_DELAY;
      tot+=val;
      cpt+=1;
    }
    Newcell=Newcell->suiv;
  }
  moy=tot/cpt;
  while (cellule_airline!=NULL)
  {
    if (strcmp((cellule_airline-> info-> iATA_CODE_AIRLINE),IATACODE)==0) printf("%s,%d\n",&(cellule_airline-> info),&(moy) );
    cellule_airline=cellule_airline->suiv;
  }
}

void changed_flight(int month,int day,int weekday,Hashtable Vols)
{
  while (cellule_airline!=NULL)
  {
    int index=hash_airline(cellule_airline-> info-> iATA_CODE_AIRLINE);
    struct cellule_vol* Newcell=Vols[index];
    while (Newcell!=NULL)
    {
      if (Newcell-> info->MONTH==month && Newcell-> info->DAY==day && Newcell-> info->WEEKDAY==weekday)
      {
        if (Newcell-> info->DIVERTED==1 || Newcell-> info->CANCELLED==1)
        {
          printf("%d,%d,%d,%s,%s,%s,%d,%d,%f,%d,%d,%f,%d,%d\n", Newcell-> info->MONTH, Newcell-> info->DAY, Newcell-> info->WEEKDAY, Newcell-> info->AIRLINE, Newcell-> info->ORGAIR, Newcell-> info-> DEST_AIR, Newcell-> info->SCHED_DEP, Newcell-> info->DEP_DELAY, Newcell-> info->AIR_TIME, Newcell-> info->DIST, Newcell-> info->SCHED_ARR, Newcell-> info->ARR_DELAY, Newcell-> info->DIVERTED, Newcell-> info->CANCELLED );
        }
      }
      Newcell=Newcell->suiv;

    }
    cellule_airline=cellule_airline->suiv;
  }
}

void avg_flight_direction(char* ITASIZE1,char* ITASIZE2,Hashtable Vols)
{
  int val=0,tot=0,cpt=0,moy=0;
  int index=get_hashed_index(ITASIZE1[4]);
  struct cellule_vol* Newcell = Vols[index];
  while (Newcell!=NULL)
  {
    if (Newcell-> info->DEST_AIR==ITASIZE2)
    {
      if (Newcell-> info->CANCELLED!=1)
      {
        val=Newcell-> info->AIR_TIME;
        tot+=val;
        cpt+=1;
      }
    }
    Newcell=Newcell->suiv;

  }
  moy=tot/cpt;
  printf("Average: %f minutes (%d flights)\n",&(moy),&(cpt) );
}

void find_itinerary(char* ITASIZE1,char* ITASIZE2,int month,int day,int weekday,int limit,Hashtable Vols) {
  int i=0;
  while (cellule_airport!=NULL)
  {

    int index=hash_airport(cellule_airport-> info-> iATA_CODE_AIRPORT);
    struct cellule_vol* Newcell=Vols[index];

    while (Newcell!=NULL || i<limit)
    {

      if ((Newcell-> info->MONTH==month) && (Newcell-> info->DAY==day) && (Newcell-> info->WEEKDAY==weekday)) {

        if (Newcell-> info->ORGAIR==ITASIZE1 && Newcell-> info->DEST_AIR==ITASIZE2)
        {
          printf("%s\n",&(Newcell-> info) );
          i++;
        }
        else
        {
          if (Newcell-> info->ORGAIR==ITASIZE1)
          {
            int indextemp=get_hashed_index(Newcell-> info->DEST_AIR);
            struct cellule_vol* temp=Vols[indextemp];

            while (temp!=NULL)
            {
              if (temp-> info->MONTH==month && temp-> info->DAY==day && temp-> info->WEEKDAY==weekday)
              {
                if ((temp-> info->DEST_AIR==ITASIZE2) && ((Newcell-> info->SCHED_ARR + Newcell-> info->ARR_DELAY)<(temp-> info->SCHED_DEP + temp-> info->DEP_DELAY)))
                {
                  printf("%s\n %s \n",&(Newcell-> info),&(temp-> info));
                  i++;
                }
              }
              temp=temp->suiv;
            }
          }
        }
      }
      Newcell=Newcell->suiv;
    }
    cellule_airport=cellule_airport->suiv;
  }
}

void find_multicity_itinerary(char ITASIZE11[4],char ITASIZE21[4],int month1,int day1,int weekday1,char ITASIZE12[4],char ITASIZE22[4],int month2,int day2,int weekday2,char ITASIZE13[4],char ITASIZE23[4],int month3,int day3,int weekday3,Hashtable Vols)
{
  find_itinerary(ITASIZE11,ITASIZE21,month1,day1,weekday1,1,Vols);
  find_itinerary(ITASIZE12,ITASIZE22,month2,day2,weekday2,1,Vols);
  find_itinerary(ITASIZE13,ITASIZE23,month3,day3,weekday3,1,Vols);

}
*/



//fonction de recherche, qui permet de savoir si le caractère en paramètre fait partie de la liste chainée, renvoie 0 si non et 1 si oui
int recherche(struct cell* liste,char* x)
{
  int existe=0;
  struct cell * p;
  p = liste;
  while ((p != NULL))
  {
    if ((strcmp((p->val),x) == 0)) existe = 1;
    p = p -> suiv;
  }
  return existe;
}



//fonction show_airport qui va afficher les aeroports dont sont originaires les vols de la compagnies passée en parametre
void show_airport(char* IATACODE,Hashtable Vols)
{
  int index = hash_index(IATACODE);
  struct cellule_vol * Newcell = malloc(sizeof(struct cellule_vol));
  Newcell = Vols[index];
  struct cell** rep = malloc(sizeof(struct cell));

  while (Newcell != NULL)
  {
    if (strcmp((Newcell -> info -> AIRLINE),IATACODE) == 0)
    {
      if (recherche(*rep, Newcell -> info -> ORGAIR) != 1)
      {
          ajout_tete(rep, Newcell -> info->ORGAIR);
          printf("%s\n", Newcell -> info->ORGAIR );
      }
    }
    Newcell = Newcell -> suiv;
  }
  //affiche_airport(IATACODE,Newcell);
}

//fonction show_airlines qui va afficher les compagnies aeriennes qui sont originaires de l'aeroport passé en parametre
void show_airlines(char* IATACODE,Hashtable Vols)
{
  int index = hash_index(IATACODE);
  struct cellule_vol * Newcell = malloc(sizeof(struct cellule_vol));
  Newcell = Vols[index];
  struct cell** rep = malloc(sizeof(struct cell));

  while (Newcell != NULL)
  {
    if (strcmp((Newcell -> info -> ORGAIR), IATACODE) == 0)
    {
      if (recherche(*rep, Newcell -> info -> AIRLINE) != 1)
      {
          ajout_tete(rep, Newcell -> info -> AIRLINE);
          printf("%s\n", Newcell -> info -> AIRLINE );
      }
    }
    Newcell = Newcell -> suiv;
  }
  //affiche_airline(IATACODE,Newcell);
}

//Fonction qui affiche les vols qui partent dee l'aéroport a la date qui est passé en parametre
void show_flight(char * IATACODE,int month,int day,int weekday,Hashtable Vols)
{
  int index = hash_index(IATACODE);
  struct cellule_vol * Newcell = malloc(sizeof(struct cellule_vol));
  Newcell = Vols[index];
  while (Newcell != NULL)
  {
    if ((Newcell -> info -> MONTH == month) && (Newcell -> info -> DAY == day) && (Newcell -> info -> WEEKDAY == weekday) && strcmp((Newcell -> info -> ORGAIR),IATACODE) == 0)
    {
      printf("%d,%d,%d,%s,%s,%s,%d,%d,%f,%d,%d,%f,%d,%d\n", Newcell -> info -> MONTH, Newcell -> info -> DAY, Newcell -> info -> WEEKDAY, Newcell -> info -> AIRLINE, Newcell -> info -> ORGAIR, Newcell -> info -> DEST_AIR, Newcell -> info -> SCHED_DEP, Newcell -> info->DEP_DELAY, Newcell -> info -> AIR_TIME, Newcell -> info -> DIST, Newcell -> info -> SCHED_ARR, Newcell -> info -> ARR_DELAY, Newcell -> info -> DIVERTED, Newcell -> info -> CANCELLED );
   }
    Newcell = Newcell -> suiv;
  }
}
