#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../includes/open.h"



void delete_flights(struct cellule_vol** pl) // POur chaque ligne de la Hashtable, on supprime la liste chainée
{

  while ((*pl)!=NULL)
    {
      struct cellule_vol* tmp;
      tmp=*pl;
      *pl=(*pl)->suiv;
      free(tmp);
    }
}

void supp_ht_flight(Hashtable ht)
{
  for(int i=0; i<MAXFLIGHTS; i++)
    {
      delete_flights(&ht[i]);
    }
}

void delete_airports(struct cellule_airport** pl)
{

  while ((*pl)!=NULL)
    {
      struct cellule_airport* tmp;
      tmp=*pl;
      *pl=(*pl)->suiv;
      free(tmp);
    }
}


void delete_airlines(struct cellule_airline** pl)
{

  while ((*pl)!=NULL)
    {
      struct cellule_airline* tmp;
      tmp=*pl;
      *pl=(*pl)->suiv;
      free(tmp);
    }
}
