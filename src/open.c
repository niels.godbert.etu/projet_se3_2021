#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXFLIGHTS 500

//////////////// Définitions des Structures de Données de chaque fichier .csv ////////////////


struct airline  // Structure des compagnies aériennes
{
  char IATA_CODE_AIRLINE[3];
  char AIRLINE[31];
};

struct airport  // Structure des aéroports
{
  char IATA_CODE_AIRPORT[4];
  char NAME[81];
  char CITY[31];
  char STATE[4];
  char COUNTRY[4];
  float LATITUDE;
  float LONGITUDE;
};

struct flight  // Structure des vols
{
  int MONTH;
  int DAY;
  int WEEKDAY;
  char AIRLINE[3];
  char ORGAIR[4];
  char DEST_AIR[4];
  int SCHED_DEP;
  int DEP_DELAY;
  float AIR_TIME;
  int DIST;
  int SCHED_ARR;
  float ARR_DELAY;
  int DIVERTED;
  int CANCELLED;
};

struct cellule_airport
{
  struct airport *info;
  struct cellule_airport *suiv;
};

struct cellule_airline
{
  struct airline *info;
  struct cellule_airline *suiv;
};

struct cellule_vol
{
  struct flight *info;
  struct cellule_vol *suiv;
};

typedef struct cellule_vol * Hashtable[MAXFLIGHTS];


void init_ht(Hashtable ht) // On initialise la table de hachage
{
  for (int i=0; i<MAXFLIGHTS; i++)  ht[i] = NULL;
}

int hash_index(char * word) //Fonction de hachage pour nos tables
{
  int index = 0;
  size_t last = strlen(word);
  for (size_t i = 0; i<last; i++)
  {
    if (i == 0) index += abs(word[i]-64);
    if (i == 1) index = abs(index-(word[i]-64));
    if (i == 2) index += abs(word[i]-64);
  }
  return index % MAXFLIGHTS;
}

//////////////// Fonctions de lecture et d'ajout (chargement) dans les SD de nos fichiers .csv ////////////////

void ajout_tete_vols(struct cellule_vol** Cell, struct flight** vol)
{
  struct cellule_vol * NewCell = calloc(1,sizeof(struct cellule_vol));
  NewCell->info = *vol;
  NewCell -> suiv = (*Cell);
  (*Cell) = NewCell;
}

void ajout_tete_airline(struct cellule_airline** Cell, struct airline* ligne)
{
  struct cellule_airline * NewCell = calloc(1,sizeof(struct cellule_airline));
  NewCell->info = ligne;
  NewCell -> suiv = (*Cell);
  (*Cell) = NewCell;
}

void ajout_tete_airport(struct cellule_airport** Cell, struct airport* aeroport)
{
  struct cellule_airport * NewCell = calloc(1,sizeof(struct cellule_airport));
  NewCell->info = aeroport;
  NewCell -> suiv = (*Cell);
  (*Cell) = NewCell;
}

void read_flights_line(struct flight** vol, char* line)
{
  struct flight* VolLu = calloc(1,sizeof(struct flight));
  char **pointline = &line;
  char* token = strsep(pointline, ",");
  int cpt = 0;
  while (token != NULL)
  {
    if (cpt == 0)  VolLu->MONTH = atoi(token);
  	if (cpt == 1)  VolLu->DAY = atoi(token);
    if (cpt == 2)  VolLu->WEEKDAY = atoi(token);
    if (cpt == 3)  strcpy(VolLu->AIRLINE, token);
    if (cpt == 4)  strcpy(VolLu->ORGAIR, token);
    if (cpt == 5)  strcpy(VolLu->DEST_AIR, token);
    if (cpt == 6)  VolLu->SCHED_DEP = atoi(token);
    if (cpt == 7)  VolLu->DEP_DELAY = atof(token);
    if (cpt == 8)  VolLu->AIR_TIME = atof(token);
    if (cpt == 9)  VolLu->DIST = atoi(token);
    if (cpt == 10) VolLu->SCHED_ARR = atoi(token);
    if (cpt == 11) VolLu->ARR_DELAY = atof(token);
    if (cpt == 12) VolLu->DIVERTED = atoi(token);
    if (cpt == 13) VolLu->CANCELLED = atoi(token);
    cpt++;
    token = strsep(pointline, ",");
  }
  *vol = VolLu;
}

void read_airline(struct airline** ligne, char* line)
{
  struct airline* LigneLue = calloc(1,sizeof(struct airline));
  char **pointline = &line;
  char* token = strsep(pointline, ",");
  int cpt = 0;
  while (token != NULL)
  {
    if (cpt == 0) strcpy(LigneLue->IATA_CODE_AIRLINE, token);
    if (cpt == 1) strcpy(LigneLue->AIRLINE, token);
    cpt++;
    token = strsep(pointline, ",");
  }
  *ligne = LigneLue;
}

void read_airport(struct airport** port, char* line)
{
  struct airport* PortLu = calloc(1,sizeof(struct airport));
  char **pointline = &line;
  char* token = strsep(pointline, ",");
  int cpt = 0;
  while (token != NULL)
  {
    if (cpt == 0) strcpy(PortLu->IATA_CODE_AIRPORT, token);
  	if (cpt == 1) strcpy(PortLu->NAME, token);
    if (cpt == 2) strcpy(PortLu->CITY, token);
    if (cpt == 3) strcpy(PortLu->STATE, token);
    if (cpt == 4) strcpy(PortLu->COUNTRY, token);
    if (cpt == 5) PortLu->LATITUDE = atof(token);
    if (cpt == 6) PortLu->LONGITUDE = atof(token);
    cpt++;
  	token = strsep(pointline, ",");
  }
  *port = PortLu;
}

void constr_flight_tab_airline(FILE *fp, Hashtable ht)
{
  struct flight** vol = calloc(1,sizeof(struct flight**));
  char* line = NULL;
  size_t len_line;
  getline(&line,&len_line,fp);
  while (getline(&line, &len_line, fp) != -1)
    {
      read_flights_line(vol, line);
      ajout_tete_vols(&(ht[hash_index((*vol)->AIRLINE)]), vol);
    }
}

void constr_flight_tab_airport(FILE *fp, Hashtable ht)
{
  struct flight** vol = calloc(1,sizeof(struct flight**));
  char* line = NULL;
  size_t len_line;
  getline(&line,&len_line,fp);
  while (getline(&line, &len_line, fp) != -1)
    {
      read_flights_line(vol, line);
      ajout_tete_vols(&(ht[hash_index((*vol)->ORGAIR)]), vol);
    }
}


void constr_airline_tab(FILE *fp, struct cellule_airline ** Airlines)
{
  struct airline* lign = calloc(1,sizeof(struct airline*));
  char* line = NULL;
  size_t len_line = 0;
  getline(&line,&len_line,fp);
  while (getline(&line, &len_line, fp) != -1)
    {
      read_airline(&lign,line);
      ajout_tete_airline(Airlines,lign);
    }
}


void constr_airport_tab(FILE *fp, struct cellule_airport ** Airports)
{
  struct airport* port = calloc(1,sizeof(struct airport*));
  char* line = NULL;
  size_t len_line = 0;
  getline(&line,&len_line,fp);
  while (getline(&line, &len_line, fp) != -1)
    {
      read_airport(&port,line);
      ajout_tete_airport(Airports,port);
     }
}
