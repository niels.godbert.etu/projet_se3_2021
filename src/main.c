#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../includes/open.h"  // Bibliothèque comprenant les fonctions nécessaire au chargement des fichiers
#include "../includes/close.h" // Bibliothèque comprenant les fonctions nécessaire à la libération de la mémoire
#include "../includes/Requete.h" // Bibliothèque comprenant les fonctions nécessaire à la réalisation des requêtes


int main (void) //int argc, char *argv[]
{
  /////// Définition et ouverture des fichiers .csv ///////

  FILE* file_airports;
  FILE* file_flights;
  FILE* file_airlines;

  file_airports=fopen("data/airports.csv","r");
  file_flights=fopen("data/flights.csv","r");
  file_airlines=fopen("data/airlines.csv","r");

  printf("Ouverture des fichiers.. \n");

  if( (file_airports == NULL) || (file_flights == NULL) || (file_airlines == NULL) )
  {
    printf("Ouverture des fichiers impossible ! \n" );
    exit(0);
  }

  printf("Fichiers bien ouverts ! \n");

  /////// Définiton et chargement des données dans les SD ///////

  Hashtable Vols_Airline; // La table de hachage qui triera les données par compagnie aérienne
  init_ht(Vols_Airline);

  Hashtable Vols_Airport; // La table de hachage qui triera les données par aéroport
  init_ht(Vols_Airport);

  constr_flight_tab_airline(file_flights,Vols_Airline);
  constr_flight_tab_airport(file_flights,Vols_Airport);

  printf("Tables de hachages pour les vols bien construites\n");

  struct cellule_airport* Airports = calloc(1, sizeof(struct cellule_airport)); // Liste chainée des aéroports
  struct cellule_airline* Airlines = calloc(1, sizeof(struct cellule_airline)); // Liste chainée des compagnies aériennes

  Airports = NULL;
  Airlines = NULL;

  constr_airport_tab(file_airports,&Airports);
  printf("Liste des aéroports bien construite \n");

  constr_airline_tab(file_airlines,&Airlines);
  printf("Liste des compagnies aériennes bien construite \n");

  /*int q=1;    // Tant que q ("quitter") est ≠0, on lit les requêtes
   while (q==1)
     {

     }*/

  /////// On teste nos fonctions, le seul parametre à modifier à chaque fois est celui entre guillemets ///////

  show_airport("ORD",Vols_Airline);
  show_airlines("EV",Vols_Airport);
  show_flight("LAX",1,1,4, Vols_Airport);

  /////// On libère la mémoire ///////

  supp_ht_flight(Vols_Airline);
  supp_ht_flight(Vols_Airport);
  delete_airlines(&Airlines);
  delete_airports(&Airports);

  /////// On ferme les fichiers ///////

  fclose(file_airports);
  fclose(file_flights);
  fclose(file_airlines);

  return 0;
}
