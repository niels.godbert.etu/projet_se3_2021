struct cell
{
  char * val;
  struct cell * suiv;
};
/*Prototype*/

void ajout_tete(struct cell **p, char *al);
int recherche(struct cell* liste,char* x);
void show_airport(char* IATACODE,Hashtable Vols);
void show_airlines(char* IATACODE,Hashtable Vols);
void show_flight(char * IATACODE,int month,int day,int weekday,Hashtable Vols);
