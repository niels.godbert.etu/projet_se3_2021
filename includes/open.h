#define MAXFLIGHTS 500
#define IATASIZEA 3
#define IATASIZEB 4

struct airline
{
  char IATA_CODE_AIRLINE[3];
  char AIRLINE[31];
};

struct airport
{
  char IATA_CODE_AIRPORT[4];
  char NAME[81];
  char CITY[31];
  char STATE[4];
  char COUNTRY[4];
  float LATITUDE;
  float LONGITUDE;
};

struct flight
{
  int MONTH;
  int DAY;
  int WEEKDAY;
  char AIRLINE[3];
  char ORGAIR[4];
  char DEST_AIR[4];
  int SCHED_DEP;
  int DEP_DELAY;
  float AIR_TIME;
  int DIST;
  int SCHED_ARR;
  float ARR_DELAY;
  int DIVERTED;
  int CANCELLED;
};



struct cellule_airport
{
  struct airport *info;
  struct cellule_airport *suiv;
};


struct cellule_airline
{
  struct airline *info;
  struct cellule_airline *suiv;
};

struct cellule_vol
{
  struct flight *info;
  struct cellule_vol *suiv;
};

typedef struct cellule_vol * Hashtable[MAXFLIGHTS];

/*
 * Prototypes
 */
void init_ht(Hashtable ht);
int hash_index(char * word);
void ajout_tete_vols(struct cellule_vol** Cell, struct flight** vol);
void ajout_tete_airline(struct cellule_airline** Cell, struct airline** ligne);
void ajout_tete_airport(struct cellule_airport** Cell, struct airport** aeroport);
void read_flights_line(struct flight** vol, char* line);
void read_airline(struct airline** ligne, char* line);
void read_airport(struct airport** port, char* line);
void constr_flight_tab_airline(FILE *fp, Hashtable ht);
void constr_flight_tab_airport(FILE *fp, Hashtable ht);
void constr_airline_tab(FILE *fp, struct cellule_airline ** Airlines);
void constr_airport_tab(FILE *fp, struct cellule_airport ** Airports);
