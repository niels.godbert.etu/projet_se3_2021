TARGET = projet
CFLAGS = -g -W -Wall -Wextra -O2 -I src
LDFLAGS = -lm
SRC = $(wildcard *.c src/*.c)
OBJ = $(SRC:.c=.o)

default: $(TARGET)	

$(TARGET): $(OBJ)
	$(CC) -o $(TARGET) $(OBJ) $(LDFLAGS)

.PHONY: clean

clean:
	rm -f *.o src/*.o
	rm -f $(TARGET)
